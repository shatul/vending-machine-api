# RESTful API in Node for VendingMachine

## Installation

Clone the repo:

```bash
git clone https://gitlab.com/shatul/vending-machine-api
cd vending-machine-api

```

Install the dependencies:

```bash
npm install
```

Set the environment variables:

```bash
cp env.example env

# open env and modify the environment variables (if needed)
```

## Commands

Running locally:

```bash
npm dev
```

Running in production:

```bash
npm start
```

## Environment Variables

The environment variables can be found and modified in the `env` file. They come with these default values:

```bash
# Port number
PORT=5000

# URL of the Mongo DB
MONGODB_URL=mongodb://127.0.0.1:27017/vendingMachineDB
```

### API Endpoints

List of available routes:

**Task routes**:\
`POST /api/inventory` - create a product\
`GET /api/inventory` - get all products\
`GET /api/inventory/:id` - get product\
`PUT /api/inventory/:id` - update product\
`DELETE /api/inventory/:id` - delete product\
`POST /api/inventory/filterProduct` - filter a product\
`POST /api/vendingMachine/buy` - buy product

### API Documentation
```
https://documenter.getpostman.com/view/8187201/TzsZroW3
```