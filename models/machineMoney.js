const mongoose = require('mongoose');

const machineMoneySchema = mongoose.Schema({
    500: {
        type: Number
    },
    200: {
        type: Number
    },
    100: {
        type: Number
    },
    50: {
        type: Number
    },
    20: {
        type: Number
    },
    10: {
        type: Number
    },
    5: {
        type: Number
    },
    2: {
        type: Number
    },
    1: {
        type: Number
    }
}
)

const MachineMoney = mongoose.model('machineMoney', machineMoneySchema)

module.exports = MachineMoney