const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 30,
    },
    SKU: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    product_quantity: {
        type: Number,
        default: 0
    },
    slot_quantity: {
        type: Number,
        default: 0
    },
    slot_capacity: {
        type: Number,
        default: 0
    }
},
    { timestamps: true }
)

const Product = mongoose.model('product', productSchema)

module.exports = Product