const Product = require('../models/productModel');
const MachineMoney = require('../models/machineMoney');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const DENOMINATIONS = [500, 200, 100, 50, 20, 10, 5, 2, 1];

exports.addMoney = catchAsync(async (req, res) => {
    const { denominations } = req.body
    let machineMoney = await MachineMoney.findOne({})
    if (machineMoney) {
        for (const iterator of DENOMINATIONS) {
            machineMoney[iterator] = denominations[iterator] ? denominations[iterator] : 0;
        }
        machineMoney = await machineMoney.save();
    } else {
        machineMoney = {};
        for (const iterator of DENOMINATIONS) {
            machineMoney[iterator] = denominations[iterator] ? denominations[iterator] : 0;
        }
        machineMoney = await MachineMoney.create(machineMoney);
    }

    res.status(httpStatus.CREATED).json({
        success: true,
        data: machineMoney,
        message: 'Money added successfully'
    });
})

exports.buyProduct = catchAsync(async (req, res) => {

    const { name, denominations } = req.body

    let amount_paid = DENOMINATIONS.reduce(
        (accumulator, currentValue) =>
            accumulator + (denominations[currentValue] ? denominations[currentValue] * currentValue : 0)
        , 0)

    console.log("amount_paid : " + amount_paid);

    let selectedProduct = await Product.findOne({ name })

    if (!selectedProduct) {
        return res.status(httpStatus.OK).json({
            success: false,
            amountToReturnWithDenominations: denominations,
            message: 'Product Not available'
        })
    }

    if (selectedProduct.product_quantity < 1) {
        return res.status(httpStatus.OK).json({
            success: false,
            amountToReturnWithDenominations: denominations,
            message: 'we are temporarily out of stock.'
        })
    }

    let changeToReturn = amount_paid - selectedProduct.price

    console.log("changeToReturn : " + changeToReturn);

    if (changeToReturn < 0) {
        return res.status(httpStatus.OK).json({
            success: false,
            amountToReturnWithDenominations: denominations,
            message: 'Insufficient Amount, Giving back the money'
        })
    }

    let machineMoney = await MachineMoney.findOne({})
    console.log(machineMoney);
    if (!machineMoney) {
        return res.status(httpStatus.OK).json({
            success: false,
            amountToReturnWithDenominations: denominations,
            message: 'machineMoney issue'
        })
    }
    // add money into machine
    for (const iterator of DENOMINATIONS) {
        machineMoney[iterator] = machineMoney[iterator] + (denominations[iterator] ? denominations[iterator] : 0);
    }

    let changeToReturnWithDenominations = {};

    if (changeToReturn > 0) {
        let amountInMachine = DENOMINATIONS.reduce((accumulator, currentValue) => accumulator + machineMoney[currentValue] * currentValue, 0)

        console.log("amountInMachine : " + amountInMachine);

        let changeAvailable = amountInMachine - changeToReturn;

        console.log("changeAvailable : " + changeAvailable);

        if (changeAvailable < 0) {
            return res.status(httpStatus.OK).json({
                success: false,
                amountToReturnWithDenominations: denominations,
                message: 'Change Not Available, Giving back the money'
            })
        }

        changeToReturnWithDenominations = change(changeToReturn, DENOMINATIONS)
        console.log("changeToReturnWithDenominations");
        console.log(changeToReturnWithDenominations);
        // remove money from machine
        for (const iterator of DENOMINATIONS) {
            machineMoney[iterator] = machineMoney[iterator] - (changeToReturnWithDenominations[iterator] ? changeToReturnWithDenominations[iterator] : 0);
            if (machineMoney[iterator] < 0) {
                // break;
                return res.status(httpStatus.OK).json({
                    success: false,
                    amountToReturnWithDenominations: denominations,
                    message: 'Change Not Available, Giving back the money'
                })
            }
        }
    }

    console.log('before save');
    console.log(machineMoney);
    machineMoney = await machineMoney.save();

    selectedProduct.slot_quantity = selectedProduct.slot_quantity - 1;
    selectedProduct.product_quantity = selectedProduct.product_quantity - 1;

    selectedProduct = await selectedProduct.save();

    if (selectedProduct.product_quantity < 0) {
        // send alert to add more products
    }

    if (selectedProduct.slot_quantity < 5) {
        // add more products to slot
    }

    return res.status(httpStatus.OK).json({
        success: true,
        amountToReturnWithDenominations: changeToReturnWithDenominations,
        message: `Your change is: ${changeToReturn}. please collect ${selectedProduct.name} from vending machine`
    })

})

function change(amount, denominations) {
    var res = {};

    for (var i = 0; amount > 0 && i < denominations.length; i++) {
        var value = denominations[i];

        if (value <= amount) {
            res[value] = Math.floor(amount / value);
            amount -= value * res[value];
        }
    }

    return res;
}
