const Product = require('../models/productModel');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');

exports.addProduct = catchAsync(async (req, res) => {
    const { name, SKU, price, product_quantity, slot_quantity, slot_capacity } = req.body
    const product = await Product.create({ name, SKU, price, product_quantity, slot_quantity, slot_capacity });
    res.status(httpStatus.CREATED).json({
        success: true,
        data: product,
        message: 'product is added successfully'
    });
})

exports.updateProduct = catchAsync(async (req, res) => {
    const { name, SKU, price, product_quantity, slot_quantity, slot_capacity } = req.body
    const existProduct = await Product.findOne({ _id: req.params.id })
    if (existProduct) {
        existProduct.name = name;
        existProduct.SKU = SKU;
        existProduct.price = price;
        existProduct.product_quantity = product_quantity;
        existProduct.slot_quantity = slot_quantity;
        existProduct.slot_capacity = slot_capacity;
        const updatedProduct = await existProduct.save();
        res.status(httpStatus.OK).json({
            success: true,
            data: updatedProduct,
            message: 'product is updated successfully'
        })
    } else {
        res.status(httpStatus.OK).json({
            success: false,
            data: null,
            message: 'product is Not Found'
        })
    }

})

exports.deleteProduct = catchAsync(async (req, res) => {
    const existProduct = await Product.findOne({ _id: req.params.id })
    if (existProduct) {
        await existProduct.remove();
        res.status(httpStatus.OK).json({
            success: true,
            message: 'product is deleted successfully'
        })
    } else {
        res.status(httpStatus.OK).json({
            success: false,
            data: null,
            message: 'product is Not Found'
        })
    }

})

exports.getSingleProduct = catchAsync(async (req, res) => {
    const existProduct = await Product.findOne({ _id: req.params.id })
    if (existProduct) {
        res.status(httpStatus.OK).json({
            success: true,
            data: existProduct,
            message: 'product is fetched successfully'
        })
    } else {
        res.status(httpStatus.OK).json({
            success: false,
            data: null,
            message: 'product is Not Found'
        })
    }

})

exports.getAllProducts = catchAsync(async (req, res) => {
    const allProducts = await Product.find({})
    if (allProducts) {
        res.status(httpStatus.OK).json({
            success: true,
            data: allProducts,
            message: 'Products Found'
        })
    } else {
        res.status(httpStatus.OK).json({
            success: false,
            data: null,
            message: 'Products are Not Found'
        })
    }

})

exports.filterProduct = catchAsync(async (req, res) => {
    const { name, SKU, price } = req.body
    const allProducts = await Product.find().or([{ name }, { SKU }, { price }])
    if (allProducts) {
        res.status(httpStatus.OK).json({
            success: true,
            data: allProducts,
            message: 'Products Found'
        })
    } else {
        res.status(httpStatus.OK).json({
            success: false,
            data: null,
            message: 'Products are Not Found'
        })
    }

})