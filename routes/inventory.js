const express = require('express');
const router = express.Router();

const { addProduct, updateProduct, deleteProduct, getSingleProduct, getAllProducts, filterProduct } = require('../controllers/inventoryController');

router.route('/').post(addProduct);
router.route('/:id').put(updateProduct);
router.route('/:id').delete(deleteProduct);
router.route('/:id').get(getSingleProduct);
router.route('/').get(getAllProducts);
router.route('/filterProduct').post(filterProduct);

module.exports = router