const express = require('express');
const router = express.Router();

const { buyProduct, addMoney } = require('../controllers/vendingMachineController');

router.route('/buy').post(buyProduct);

router.route('/addMoney').post(addMoney);


module.exports = router